conda install anaconda

# Stable environment -
# Updates all packages to latest 'releases', that work well together

conda update conda
conda update anaconda

# Unstable environment -
# Updates all packages to latest 'releases'

conda update --all
